<?php

declare(strict_types=1);

namespace MyTarget\Message;

final class ErrorMessage
{
    private string $guid;
    private string $errorText;

    public function __construct(string $guid, string $errorText)
    {
        $this->guid = $guid;
        $this->errorText = $errorText;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getErrorText(): string
    {
        return $this->errorText;
    }
}
