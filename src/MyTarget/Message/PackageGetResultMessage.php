<?php


namespace MyTarget\Message;


use MyTarget\ResultFields\Package\Package;

final class PackageGetResultMessage extends BaseResultMessage
{
    /** @var array|Package[] */
    private array $packages;

    public function __construct(array $packages, string $guid)
    {
        parent::__construct($guid);
        $this->packages = $packages;
    }

    /**
     * @return array|Package[]
     */
    public function getPackages():array
    {
        return $this->packages;
    }
}