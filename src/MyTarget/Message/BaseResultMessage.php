<?php

declare(strict_types=1);

namespace MyTarget\Message;

abstract class BaseResultMessage
{
    private string $guid;

    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }
}
