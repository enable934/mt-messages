<?php


namespace MyTarget\Message;


final class PackageGetMessage extends BaseCreateMessage
{
    private string $clientName;

    public function __construct(string $clientName, string $guid)
    {
        parent::__construct($guid);
        $this->clientName = $clientName;
    }

    public function getClientName(): string
    {
        return $this->clientName;
    }
}