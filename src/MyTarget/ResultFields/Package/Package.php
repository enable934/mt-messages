<?php

namespace MyTarget\ResultFields\Package;

final class Package
{
    private int $id;
    private string $description;
    private int $maxUniqShowsLimit;
    private string $name;
    private int $price;

    public function __construct(int $id, string $description, int $maxUniqShowsLimit, string $name, int $price)
    {
        $this->id = $id;
        $this->description = $description;
        $this->maxUniqShowsLimit = $maxUniqShowsLimit;
        $this->name = $name;
        $this->price = $price;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getMaxUniqShowsLimit(): int
    {
        return $this->maxUniqShowsLimit;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}